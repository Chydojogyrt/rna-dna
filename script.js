var rna1 = '';
var aminName = '';
var aminCode = '';

/******************************/

var randomRna = function(howMany) {

while(rna1.length < (howMany*3)){
	nuclioacidNumber = Math.floor(Math.random()*4);
	switch(nuclioacidNumber){
		case 0:
		rna1 = rna1 + 'u'
		break;
		case 1:
		rna1 = rna1 + 'c'
		break;
		case 2:
		rna1 = rna1 + 'a'
		break;
		case 3:
		rna1 = rna1 + 'g'
		break;
			}
}
	 return rna1;
};

/***********************************/

var transformItToAA = function(rna){

	var triplets = ['asp ', 'ala ', 'arg ', 'asn ', 
					'val ', 'gis ', 'gli ', 'gln ', 
					'gly ', 'ile ', 'lei ', 'liz ', 
					'met ', 'pro ', 'ser ', 'tir ', 
					'tre ', 'tri ', 'fen ', 'cis ', '-st '
					];

	var groups = [
				 	[
				 		[triplets[18], triplets[18], triplets[10], triplets[18]],
				 		[triplets[14], triplets[14], triplets[14], triplets[14]],
				 		[triplets[15], triplets[15], triplets[20], triplets[20]],
				 		[triplets[19], triplets[19], triplets[20], triplets[17]]
				 	],
					[
						[triplets[10], triplets[10], triplets[10], triplets[10]],
						[triplets[13], triplets[13], triplets[13], triplets[13]],
						[triplets[5], triplets[5], triplets[7], triplets[7]],
						[triplets[2], triplets[2], triplets[2], triplets[2]]
					], 
					[
						[triplets[9], triplets[9], triplets[9], triplets[12]],
						[triplets[16], triplets[16], triplets[16], triplets[16]],
						[triplets[3], triplets[3], triplets[11], triplets[11]],
						[triplets[14], triplets[14], triplets[2], triplets[2]]
					], 
					[
						[triplets[4], triplets[4], triplets[4], triplets[4]],
						[triplets[1], triplets[1], triplets[1], triplets[1]],
						[triplets[0], triplets[0], triplets[8], triplets[8]],
						[triplets[6], triplets[6], triplets[6], triplets[6]]
					]
				 ];

	var groupCheck = function(code){
		var groupNumber;
		switch(code){
			case 'u':
			groupNumber = 0
		break;
			case 'c':
			groupNumber = 1
		break;
			case 'a':
			groupNumber = 2
		break;
			case 'g':
			groupNumber = 3
		break;
		}
		return groupNumber;
	}

	var instr = 0;
	var outstr = 3;

	while(aminName.length < ((rna.length/3)*4)){
			
		aminCode = rna.substring(instr, outstr);

		aminName = aminName + groups[groupCheck(aminCode[0])][groupCheck(aminCode[1])][groupCheck(aminCode[2])];
		
		instr = instr + 3;
		outstr = outstr + 3;
	}
	return aminName;
};




var layout = function(){

	var rnaDiv = document.createElement("div");
    document.body.appendChild(rnaDiv).innerHTML = rna1; 
    var aminDiv = document.createElement("div");
    document.body.appendChild(aminDiv).innerHTML = aminName;


}
